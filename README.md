## YU - GI- OH! DECK


### What does it do?
- Displays a list of the cards and their description
- Provides filtering of the cards based on their name
- Provides filtering of the cards based on their type

### How to install

First, you need [Node.js](https://nodejs.org/) to be installed in your dev environment as the project depends on it. Next, you should clone the project to your dev environment:
    
    git clone https://bitbucket.org/natalia_markaki/yu-gi-oh-deck
    
Once the project is cloned you can use the following commands to run and test the project:

- **npm start**
- **npm run test** (will start karma for countinous testing once code changes are detected)
- **npm run test-single-run** (will start karma for one single test run)

Above commands are defined in the **package.json** configuration file in the root of the project.