'use strict';
var deckGame = angular.module('deckGame', ['ui.router', 'deckGame.list', 'deckGame.images']);

deckGame.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('#', {
			url: '/',
			views: {
				// the main template will be placed here
				'': { templateUrl: 'components/views/cardListView.html' },

				'column-list@#': { 
					templateUrl: 'components/views/cardsView.html',
					controller: 'cardListCtrl'
				 },

				'column-details@#': { 
					templateUrl: 'components/views/cardDetailsView.html',
				}
			}
	})
	.state('image', {
			url: '/images',
			controller: 'cardImagesCtrl',
			templateUrl: 'components/views/cardImagesView.html'
	});

	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];

});