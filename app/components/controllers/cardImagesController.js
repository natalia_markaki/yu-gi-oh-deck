'use strict';
angular.module('deckGame.images', [])

// Controller definition for this module
.controller('cardImagesCtrl', ['$rootScope','$scope','$http', function($rootScope,$scope,$http) {

	$rootScope.modalDisabled = function() {
		$scope.modalEnabled = false;
	}

  $rootScope.cardDescr = function(card){
		$rootScope.current = card;
		$scope.modalEnabled = true;
		$http.get('http://52.57.88.137/api/card_data/'+ card)
			.then(function(response) {
					$scope.cardDesc = response.data.data;
			}, function(response) {
				//Second function handles error
				$rootScope.content = 'Something went wrong';
		 });
	}

	$rootScope.cardsInfo = function(type) {
		$scope.filtered = [];
		if (type === 'spell') $rootScope.cards.forEach(elementSpell);
		else if (type === 'all') $scope.filtered = $rootScope.cards;
		else if (type === 'trap') $rootScope.cards.forEach(elementTrap);
		else $rootScope.cards.forEach(elementOther);
	}

	function elementSpell(element) {
  	$http.get('http://52.57.88.137/api/card_data/'+ element)
			.then(function(response) {
					if(response.data.data.card_type === 'spell') {
						$scope.filtered.push(response.data.data);
					}
			}, function(response) {
				//Second function handles error
				$rootScope.content = 'Something went wrong';
		 });
	}

	function elementTrap(element) {
  	$http.get('http://52.57.88.137/api/card_data/'+ element)
			.then(function(response) {
					if(response.data.data.card_type === 'trap') {
						$scope.filtered.push(response.data.data);
					}
			}, function(response) {
				//Second function handles error
				$rootScope.content = 'Something went wrong';
		 });
		 $scope.content = 'Finished elementTrap';
	}

	function elementOther(element) {
  	$http.get('http://52.57.88.137/api/card_data/'+ element)
			.then(function(response) {
					if(response.data.data.card_type !== 'trap' && response.data.data.card_type !== 'spell') {
						$scope.filtered.push(response.data.data);
					}
			}, function(response) {
				//Second function handles error
				$rootScope.content = 'Something went wrong';
		 });
	}

	this.message = 'cardImagesController is defined and has its properties';
		
}]);
