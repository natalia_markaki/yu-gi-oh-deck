
'use strict';
angular.module('deckGame.list', [])

// Controller definition for this module
.controller('cardListCtrl', ['$rootScope','$http', '$anchorScroll', '$location', '$window' , function($rootScope, $http, $anchorScroll, $location, $window) {

	// In the init method we are declaring all the
	// neccesarry settings and assignments to be run once
	// controller is invoked
	init();

	function init(){
		$rootScope.cards = [
			'Burial from a Different Dimension',
			'Charge of the Light Brigade',
			'Infernoid Antra',
			'Infernoid Attondel',
			'Infernoid Decatron',
			'Infernoid Devyaty',
			'Infernoid Harmadik',
			'Infernoid Onuncu',
			'Infernoid Patrulea',
			'Infernoid Pirmais',
			'Infernoid Seitsemas',
			'Lyla, Lightsworn Sorceress',
			'Monster Gate',
			'One for One',
			'Raiden, Hand of the Lightsworn',
			'Reasoning',
			'Time-Space Trap Hole',
			'Torrential Tribute',
			'Upstart Goblin',
			'Void Seer'
		];

		cardDescription();

	};

	function cardDescription() {
		$rootScope.cardData = [];
		$rootScope.cards.forEach(getData);
	}
	
	function getData(element) {
		$http.get('http://52.57.88.137/api/card_data/'+ element, { cache: true})
			.then(function(response) {
					$rootScope.cardData.push(response.data.data);
			}, function(response) {
				$rootScope.content = 'Something went wrong';
		 });
	}

 $rootScope.cardDetails = function(card){
		$rootScope.current = card;
		$http.get('http://52.57.88.137/api/card_data/'+ card)
			.then(function(response) {
					$rootScope.cardDesc = response.data.data;
			}, function(response) {
				//Second function handles error
				$rootScope.content = 'Something went wrong';
		 });
	}

	$rootScope.gotoBottom = function() {
		if($window.innerWidth >= 800) {
			return;
		}

		$location.hash('card');
		$anchorScroll();
  };

	this.message = 'cardListController is defined and has its properties';
		
}]);
