module.exports = {
  "files": ["./**/*.{html,htm,css,js,scss}"],
  "watchOptions": { "ignored": "node_modules"  },
  "server": { "baseDir": "./app" }
};