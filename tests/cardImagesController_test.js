'use strict';

describe('cardImagesController Tests :', function() {

  // Only loading one module which is being tested
  beforeEach(module('deckGame.images'));

  describe('Loading cardImagesController. It...', function(){
    var root;
    var controller;
    var scope;
  
    beforeEach(inject(function($controller, $rootScope, $http){
      root = $rootScope.$new();
      scope = $rootScope.$new();
      controller = $controller('cardImagesCtrl', {$rootScope: root, $scope: scope, $http: $http});

    }));


    it('should be defined', inject(function() {
      expect(controller).toBeDefined();
    
    }));

    it('should have property next defined', inject(function() {
      expect(controller.message).toEqual('cardImagesController is defined and has its properties');
    
    }));

    it('should have property next defined', inject(function() {
      root.cards = [
      'Time-Space Trap Hole',
			'Torrential Tribute',
			'Upstart Goblin',
			'Void Seer'
      ];

      root.cardsInfo('trap');
    
      expect(scope.content).toEqual('Finished elementTrap');
    
    }));
    
  });
});