'use strict';

describe('cardListController Tests :', function() {

  // Only loading one module which is being tested
  beforeEach(module('deckGame.list'));

  describe('Loading cardListController. It...', function(){
    var root;
    var scope;
    var controller;

    beforeEach(inject(function($controller,$rootScope,$http,$anchorScroll, $location, $window){
      root = $rootScope.$new();
      controller = $controller('cardListCtrl', {$rootScope: root, $http: $http, $anchorScroll: $anchorScroll, $location: $location, $window: $window});

    }));


    it('should be defined', inject(function() {
      expect(controller).toBeDefined();
    
    }));

    it('should have property next defined', inject(function() {
      expect(controller.message).toEqual('cardListController is defined and has its properties');
    
    }));
  });
});